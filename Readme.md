# [Moralis Cloud Function](https://docs.moralis.io/moralis-server/cloud-functions) Boilerplate

Uses esbuild for fast bundling and uploads the bundle to Moralis.
- allows to structure your functions into separate files
- adds Typescript support (except Moralis is [missing typings](https://github.com/MoralisWeb3/issue-tracker/issues/42))
- allows to use (small) npm packages

## Setup

1. Clone this repo
2. Run `yarn` to install packages
3. Copy `.example.env` to `.env` & add Moralis CLI credentials

## Usage

1. Run `yarn dev` to build & upload and watch for changes

### NPM Packages
This setup also allows you to import npm packages, but **be aware of the bundle size**.

Now that we're already talking about bundle size:  
You can analyze your bundle by uploading `dist/meta.json` to [Bundle Buddy](https://www.bundle-buddy.com/)
